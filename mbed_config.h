// Automatically generated configuration file.
// DO NOT EDIT, content will be overwritten.

#ifndef __MBED_CONFIG_DATA__
#define __MBED_CONFIG_DATA__

// Configuration parameters
#define IN1                                         p12  // set by application[LPC1768]
#define MBED_CONF_FILESYSTEM_PRESENT                1    // set by library:filesystem
#define IN3                                         p15  // set by application[LPC1768]
#define MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE 9600 // set by library:platform
#define IN4                                         p16  // set by application[LPC1768]
#define MBED_CONF_PLATFORM_STDIO_CONVERT_NEWLINES   0    // set by library:platform
#define MBED_CONF_PLATFORM_STDIO_BAUD_RATE          9600 // set by library:platform
#define IN2                                         p13  // set by application[LPC1768]
#define MBED_CONF_PLATFORM_STDIO_FLUSH_AT_EXIT      1    // set by library:platform
// Macros
#define NDEBUG                                      1    // defined by application

#endif
