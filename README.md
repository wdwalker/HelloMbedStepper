# HelloMbedStepper

## Overview
HelloStepper is a simple stepper motor contoller for the mbed environment. It was written mostly as a learning exercise.

This will conditionally compile for the [LPC1768](https://developer.mbed.org/platforms/mbed-LPC1768/), [NUCLEO\_F042K6](https://developer.mbed.org/platforms/ST-Nucleo-F042K6/), and [RBLAB\_BLENANO](https://developer.mbed.org/platforms/RedBearLab-BLE-Nano/). Use any of these as the "-m" value when compiling.

The stepper motor used is the [Elegoo 5 sets 28BYJ-48 ULN2003 5V Stepper Motor + ULN2003 Driver Board for Arduino](http://a.co/gATnQmX).

## Download, Build, Deploy
Mac OS X:
```
mbed import git@github.com:wdwalker/HelloMbedStepper
cd HelloMbedStepper
mbed compile -m LPC1768 -t GCC_ARM && cp BUILD/LPC1768/GCC_ARM/HelloMbedStepper.bin /Volumes/MBED
```

## Sample Output (Oscilloscope)

###StepperMotor::HALF_STEP

![StepperMotor::HALF_STEP](docs/HALF_STEP.png)

###StepperMotor::FULL_STEP

![StepperMotor::FULL_STEP](docs/FULL_STEP.png)

###StepperMotor::WAVE_DRIVE

![StepperMotor::WAVE_DRIVE](docs/WAVE_DRIVE.png)
